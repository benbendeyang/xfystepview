# XFYStepView

[![CI Status](https://img.shields.io/travis/leonazhu/XFYStepView.svg?style=flat)](https://travis-ci.org/leonazhu/XFYStepView)
[![Version](https://img.shields.io/cocoapods/v/XFYStepView.svg?style=flat)](https://cocoapods.org/pods/XFYStepView)
[![License](https://img.shields.io/cocoapods/l/XFYStepView.svg?style=flat)](https://cocoapods.org/pods/XFYStepView)
[![Platform](https://img.shields.io/cocoapods/p/XFYStepView.svg?style=flat)](https://cocoapods.org/pods/XFYStepView)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

XFYStepView is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'XFYStepView'
```

## Author

leonazhu, 412038671@qq.com

## License

XFYStepView is available under the MIT license. See the LICENSE file for more info.
