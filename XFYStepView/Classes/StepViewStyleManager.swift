//
//  StepViewStyleManager.swift
//  Pods-XFYStepView_Example
//
//  Created by 🐑 on 2019/4/17.
//

import Foundation

open class StepViewStyleManager {
    
    static let bundle: Bundle = {
        guard let url = Bundle(for: StepViewStyleManager.self).url(forResource: "XFYStepView", withExtension: "bundle"), let bundle = Bundle(url: url) else {
            return Bundle.main
        }
        return bundle
    }()
    
    public static let shared = StepViewStyleManager()
    
    open var normalImage: UIImage = UIImage(named: "step_normal", in: bundle, compatibleWith: nil) ?? UIImage()
    open var highlightImage: UIImage = UIImage(named: "step_highlight", in: bundle, compatibleWith: nil) ?? UIImage()
    open var finishImage: UIImage = UIImage(named: "step_finish", in: bundle, compatibleWith: nil) ?? UIImage()
    
    open var selectedColor: UIColor = #colorLiteral(red: 0.9971458316, green: 0.65471071, blue: 0.2972868383, alpha: 1)
    open var normalColor: UIColor = #colorLiteral(red: 0.7333333333, green: 0.7333333333, blue: 0.7333333333, alpha: 1)
    
    open var stepNumberColor: UIColor = .white
    open var stepNumberFont: UIFont = .systemFont(ofSize: 23)
    
    open var stepTipColor: UIColor = #colorLiteral(red: 0.4, green: 0.4, blue: 0.4, alpha: 1)
    open var stepTipFont: UIFont = .systemFont(ofSize: 14)
    
    public func setStyle(normalImage: UIImage? = nil, highlightImage: UIImage? = nil, finishImage: UIImage? = nil, selectedColor: UIColor? = nil, normalColor: UIColor? = nil, stepNumberColor: UIColor? = nil, stepNumberFont: UIFont? = nil, stepTipColor: UIColor? = nil, stepTipFont: UIFont? = nil) {
        if let image = normalImage {
            self.normalImage = image
        }
        if let image = highlightImage {
            self.highlightImage = image
        }
        if let image = finishImage {
            self.finishImage = image
        }
        if let color = selectedColor {
            self.selectedColor = color
        }
        if let color = normalColor {
            self.normalColor = color
        }
        if let color = stepNumberColor {
            self.stepNumberColor = color
        }
        if let font = stepNumberFont {
            self.stepNumberFont = font
        }
        if let color = stepTipColor {
            self.stepTipColor = color
        }
        if let font = stepTipFont {
            self.stepTipFont = font
        }
    }
}
