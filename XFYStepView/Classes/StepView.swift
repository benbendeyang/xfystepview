//
//  StepView.swift
//  Pods-XFYStepView_Example
//
//  Created by 🐑 on 2019/4/16.
//

import UIKit

open class StepView: UIView {
    
    let bundle: Bundle = {
        guard let url = Bundle(for: StepView.self).url(forResource: "XFYStepView", withExtension: "bundle"), let bundle = Bundle(url: url) else {
            return Bundle.main
        }
        return bundle
    }()
    
    public var titles: [String] = [] {
        didSet {
            mainCollectionView.reloadData()
        }
    }
    public var currentProgress: Int = 0 {
        didSet {
            mainCollectionView.reloadData()
        }
    }
    
    private lazy var mainCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .horizontal
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.backgroundColor = .clear
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.showsVerticalScrollIndicator = false
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.register(UINib(nibName: "StepViewCell", bundle: bundle), forCellWithReuseIdentifier: "StepViewCell")
        return collectionView
    }()
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        initView()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initView()
    }
    
    override open func awakeFromNib() {
        super.awakeFromNib()
        initView()
    }
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        mainCollectionView.frame = bounds
        mainCollectionView.reloadData()
    }
    
    private func initView() {
        addSubview(mainCollectionView)
    }
}

// MARK: - 协议
// MARK: UICollectionView
extension StepView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    public func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return titles.count
    }
    
    public func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "StepViewCell", for: indexPath) as! StepViewCell
        let index = indexPath.row
        var image = StepViewStyleManager.shared.normalImage
        var progress = "\(index + 1)"
        let title = titles[index]
        var leftColor: UIColor?
        var rightColor: UIColor?
        if index == currentProgress {
            image = StepViewStyleManager.shared.highlightImage
            leftColor = StepViewStyleManager.shared.selectedColor
            rightColor = StepViewStyleManager.shared.normalColor
        } else if index < currentProgress {
            image = StepViewStyleManager.shared.finishImage
            progress = ""
            leftColor = StepViewStyleManager.shared.selectedColor
            rightColor = StepViewStyleManager.shared.selectedColor
        } else {
            leftColor = StepViewStyleManager.shared.normalColor
            rightColor = StepViewStyleManager.shared.normalColor
        }
        if index == 0 {
            leftColor = nil
        }
        if index == (titles.count - 1) {
            rightColor = nil
        }
        cell.show(image: image, progress: progress, title: title, leftColor: leftColor, rightColor: rightColor)
        return cell
    }
    
    public func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width
        let height = collectionView.bounds.height
        return CGSize(width: width/CGFloat(titles.count), height: height)
    }
}

