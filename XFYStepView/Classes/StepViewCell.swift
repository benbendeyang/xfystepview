//
//  StepViewCell.swift
//  Pods-XFYStepView_Example
//
//  Created by 🐑 on 2019/4/16.
//

import UIKit

class StepViewCell: UICollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var progressLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var leftLine: UIView!
    @IBOutlet weak var rightLine: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        progressLabel.textColor = StepViewStyleManager.shared.stepNumberColor
        progressLabel.font = StepViewStyleManager.shared.stepNumberFont
        titleLabel.textColor = StepViewStyleManager.shared.stepTipColor
        titleLabel.font = StepViewStyleManager.shared.stepTipFont
    }
    
    func show(image: UIImage, progress: String, title: String, leftColor: UIColor?, rightColor: UIColor?) {
        imageView.image = image
        progressLabel.text = progress
        titleLabel.text = title
        leftLine.backgroundColor = leftColor
        rightLine.backgroundColor = rightColor
    }
}
