//
//  ViewController.swift
//  XFYStepView
//
//  Created by leonazhu on 04/16/2019.
//  Copyright (c) 2019 leonazhu. All rights reserved.
//

import UIKit
import XFYStepView

class ViewController: UIViewController {

    @IBOutlet weak var stepView: StepView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        initView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func lastStep(_ sender: Any) {
        stepView.currentProgress = max(0, stepView.currentProgress - 1)
    }
    @IBAction func nextStep(_ sender: Any) {
        stepView.currentProgress = min(stepView.titles.count - 1, stepView.currentProgress + 1)
    }
    @IBAction func randomStepView(_ sender: Any) {
        randomCreate()
    }
}

// MARK: - 私有
private extension ViewController {
    
    func initView() {
        randomCreate()
    }
    
    func randomCreate() {
        let count = Int(arc4random()%6) + 1
        var titles = [String]()
        for i in 0..<count {
            titles.append("第\(i + 1)项")
        }
        stepView.titles = titles
        stepView.currentProgress = Int(arc4random()%UInt32(count))
        print("创建\(count)项,当前第\(stepView.currentProgress + 1)")
    }
}
